import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {PageHeader, Form, Col, FormControl, FormGroup, Button, Panel, Image} from 'react-bootstrap';
import Preloader from './assets/Preloader.gif';
import './App.css';
import {fetchMessages, sendInfo} from "./store/action";

const URL = 'http://localhost:8000/';

const styles = {
    width: '100px',
    height: '100px',
    marginRight: '10px'
};

class App extends Component {
    state = {
        author: '',
        message: '',
        image: ''
    };

    componentDidMount() {
        this.props.fetchMessages();
    }

    inputChangeHandler = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    fileChangeHandler = event => {
        this.setState({[event.target.name]: event.target.files[0]});
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    render() {
        return (
            <Fragment>
                <img className="Preloader" style={{display: this.props.loading ? 'block' : 'none'}} src={Preloader} alt="loading"/>
                <PageHeader style={{textAlign: 'center'}}>
                    Homework-78
                </PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormGroup controlId="productTitle">
                        <Col sm={4}>
                            <FormControl
                                type="text"
                                placeholder="Author"
                                name="author"
                                value={this.state.author}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                        <Col sm={4}>
                            <FormControl
                                type="text" required
                                placeholder="Message"
                                name="message"
                                value={this.state.message}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                        <Col sm={3}>
                            <FormControl
                                type="file"
                                placeholder="Add file"
                                name="image"
                                onChange={this.fileChangeHandler}
                            />
                        </Col>
                        <Col sm={1}>
                            <Button bsStyle="primary" type="submit">Send</Button>
                        </Col>
                    </FormGroup>
                </Form>
                {this.props.messages.map(message => (
                    <Panel key={message.id}>
                        <Panel.Body>
                            {message.image && <Image style={styles} src={URL + 'uploads/' + message.image} thumbnail/>}
                            <h4>{message.author}</h4>
                            <p>{message.message}</p>
                        </Panel.Body>
                    </Panel>
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        messages: state.messages,
        loading: state.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: formData => dispatch(sendInfo(formData)),
        fetchMessages: () => dispatch(fetchMessages())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
