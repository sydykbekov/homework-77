import axios from 'axios';

export const START_REQUEST = 'START_REQUEST';
export const ERROR_REQUEST = 'ERROR_REQUEST';
export const SUCCESS_REQUEST = 'SUCCESS_REQUEST';

export const startRequest = () => {
    return {type: START_REQUEST};
};

export const errorRequest = () => {
    return {type: ERROR_REQUEST};
};

export const successRequest = messages => {
    return {type: SUCCESS_REQUEST, messages};
};

export const sendInfo = formData => {
    return dispatch => {
        axios.post('messages', formData).then(() => {
            dispatch(fetchMessages());
        }, error => {
            dispatch(errorRequest());
        })
    };
};

export const fetchMessages = () => {
    return dispatch => {
        dispatch(startRequest());
        axios.get('messages').then(response => {
            dispatch(successRequest(response.data));
        }, error => {
            dispatch(errorRequest());
        });
    };
};