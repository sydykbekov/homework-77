import {START_REQUEST, ERROR_REQUEST, SUCCESS_REQUEST} from "./action";
const initialState = {
    messages: [],
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case START_REQUEST:
            return {...state, loading: true};
        case ERROR_REQUEST:
            return {...state, loading: false};
        case SUCCESS_REQUEST:
            return {...state, messages: action.messages, loading: false};
        default:
            return state;
    }
};

export default reducer;