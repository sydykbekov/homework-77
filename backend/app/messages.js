const express = require('express');
const multer = require('multer');
const router = express.Router();
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = db => {

    router.get('/', (req, res) => {
        res.send(db.getData());
    });

    router.post('/', upload.single('image'), (req, res) => {
        const message = req.body;

        if (req.file) {
            message.image = req.file.filename;
        } else {
            message.image = null;
        }

        if (message.author === '') {
            message.author = 'Anonymous';
        }

        if (message.message === '') {
            res.status(400).send({"error": "Empty message!"})
        } else {
            db.addMessage(message).then(result => {
                res.send(result);
            });
        }
    });

    return router;
};

module.exports = createRouter;