const fs = require('fs');
const nanoid = require('nanoid');

let data = null;

module.exports = {
    init: () => {
        return new Promise((resolve, reject) => {
            fs.readFile('./db.json', (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    data = JSON.parse(result);
                    resolve();
                }
            });
        });
    },
    getData: () => data,
    addMessage: message => {
        message.id = nanoid();
        data.push(message);

        let messages = JSON.stringify(data, null, 2);

        return new Promise((resolve, reject) => {
            fs.writeFile('./db.json', messages, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve(message);
                }
            });
        });
    }
};